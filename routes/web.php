<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\AnalyseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SearchController::class, 'Index'])->name('search-screen');
Route::get('getBrands', [SearchController::class, 'getBrands']);
Route::get('getBrandModels', [SearchController::class, 'getBrandModels']);
Route::get('getModelBuilds', [SearchController::class, 'getModelBuilds']);
Route::get('getMcScarcity', [SearchController::class, 'getMcScarcity']);

Route::post('analyse', [AnalyseController::class, 'searchMotorcycle'])->name('search-result');
Route::get('analyse/csv', [AnalyseController::class, 'createCsv'])->name('csv');
Route::post('analyse/pdf', [AnalyseController::class, 'createPDF'])->name('pdf');
Route::get('analyse', function () {
    return redirect()->to('/');
});
