function CalcScore(oLimit, sLimit)
{
    //Set Variables
    let calculatedScore;

    //Set input values
    if (!oLimit) {
        oLimit = 7;
        document.getElementById("iOnlineLim").value = oLimit;
    }
    if (!sLimit) {
        sLimit = 3;
        document.getElementById("iSoldLim").value = sLimit;
    } else {
        document.getElementById("iOnlineLim").value = oLimit;
        document.getElementById("iSoldLim").value = sLimit;
    }

    //Get amount of records
    let amountOfClasses = document.querySelectorAll(".data-row").length;

    //Loop through amount of rows; for each row fill data
    for (let i = 0; i < amountOfClasses; i++) {
        //Collect all information for calculations
        let amountOnline = document.querySelectorAll(".amountOnline");
        let amountSold = document.querySelectorAll(".amountSold");
        let difference = document.querySelectorAll(".difference");
        let score = document.querySelectorAll(".score");
        let currentAvgPrice = document.querySelectorAll(".average");

        //Set variables for score calculations
        //Can't be true or false since they need to be added up
        let bigDifference = 0;
        let zeroOnline = 0;
        let sold = 0;
        let online = 1;

        //Check if bigDifference is true or false
        if (Number(difference[i].innerHTML) < 1) {
            bigDifference = 1;
        }

        //Check if zeroOnline is true or false
        if (!Number(amountOnline[i].innerHTML)) {
            zeroOnline = 1;
        }

        //Check if @Verkocht is true or false
        if (Number(amountSold[i].innerHTML) > Number(sLimit)) {
            sold = 1;
        }

        //Check is @Online is true or false
        if (Number(amountOnline[i].innerHTML) > Number(oLimit)) {
            online = -1;
        }

        //Get score
        if (
            (Number(currentAvgPrice[i].innerHTML) === 0 &&
                Number(amountOnline[i].innerHTML) === 0) ||
            (Number(currentAvgPrice[i].innerHTML) === undefined &&
                Number(amountOnline[i].innerHTML) === 0)
        ) {
            calculatedScore = 3;
        } else {
            //Count score and color table cell accordingly
            calculatedScore = bigDifference + zeroOnline + online + sold;

            if (calculatedScore > 1) {
                score[i].classList.remove("scoreOrange");
                score[i].classList.remove("scoreRed");
                score[i].classList.remove("scoreGreen");
                score[i].classList.add("scoreGreen");
                score[i].innerHTML = calculatedScore;
            } else if (calculatedScore === 1) {
                score[i].classList.remove("scoreGreen");
                score[i].classList.remove("scoreRed");
                score[i].classList.remove("scoreOrange");
                score[i].classList.add("scoreOrange");
                score[i].innerHTML = calculatedScore;
            } else {
                score[i].classList.remove("scoreOrange");
                score[i].classList.remove("scoreGreen");
                score[i].classList.remove("scoreRed");
                score[i].classList.add("scoreRed");
                score[i].innerHTML = calculatedScore;
            }
        }
    }
}

function ChangeLimit()
{
    let newSoldLimit = document.getElementById("iSoldLim").value,
        newOnlineLimit = document.getElementById("iOnlineLim").value;

    CalcScore(newOnlineLimit, newSoldLimit);
}

function CalcPrices()
{
    //Set variables
    let rent, margin, resale, minResalePr;

    //Set variables and get values
    let buyInPrice = Number(document.getElementById("iBuyIn").value),
        transport = Number(document.getElementById("iTrans").value),
        mpoProvision = Number(
            document.getElementById("mpoProvision").innerHTML
        );

    //If empty/not a number set number to zero
    if (isNaN(buyInPrice)) {
        buyInPrice = 0;
    }
    if (isNaN(transport)) {
        transport = 0;
    }

    //Calculate rent
    rent = buyInPrice + transport;
    rent = rent / 100;
    rent = rent * 5;

    //Calculate margin
    margin = buyInPrice + transport + rent;
    margin = margin * 0.3;

    //Calculate minimal resale price
    resale = buyInPrice + rent + transport + margin + mpoProvision;

    //Calculate minimal resale percentage
    minResalePr =
        (resale / Number(document.getElementById("totResale").innerHTML)) * 100;

    //Write to page
    document.getElementById("minResale").innerHTML =
        parseFloat(resale).toFixed(2);
    document.getElementById("minResaleInput").value =
        parseFloat(resale).toFixed(2);
    document.getElementById("iRentAYear").innerHTML =
        parseFloat(rent).toFixed(2);
    document.getElementById("iRentAYearInput").value =
        parseFloat(rent).toFixed(2);
    document.getElementById("margin").innerHTML = parseFloat(margin).toFixed(2);
    document.getElementById("marginInput").value =
        parseFloat(margin).toFixed(2);
    document.getElementById("minResalePrcUp").innerHTML =
        parseFloat(minResalePr).toFixed(2) + "%";
    document.getElementById("minResalePrcUpInput").value =
        parseFloat(minResalePr).toFixed(2);
    document.getElementById("minResalePrc").innerHTML =
        parseFloat(minResalePr).toFixed(2) + "%";
    document.getElementById("minResalePrcInput").value =
        parseFloat(minResalePr).toFixed(2);
}

function SortTable(n)
{
    //Get table by id; Set switching to true; Set direction(dir) to asc(ascending)
    let table,
        rows,
        rowsLength,
        switching,
        i,
        currentRow,
        nextRow,
        shouldSwitch,
        direction,
        switchcount = 0;

    table = document.getElementById("table");
    switching = true;
    direction = "asc";

    while (switching) {
        //Set switching to false; Get rows
        switching = false;
        rows = table.rows;
        rowsLength = rows.length;

        for (i = 1; i < rowsLength - 1; i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;

            //Get current row and next to compare
            currentRow = rows[i].getElementsByTagName("td")[n];
            nextRow = rows[i + 1].getElementsByTagName("td")[n];

            //Check if the two rows should switch place
            if (direction === "asc") {
                if (
                    currentRow.innerHTML.toLowerCase() >
                    nextRow.innerHTML.toLowerCase()
                ) {
                    // If so, mark as a switch and break the loop and move on to next row
                    document.getElementById("sortBy").value = "asc";
                    document.getElementById("row").value = "Onderdeelcode";
                    shouldSwitch = true;
                    break;
                }
            } else if (direction === "desc") {
                if (
                    currentRow.innerHTML.toLowerCase() <
                    nextRow.innerHTML.toLowerCase()
                ) {
                    // If so, mark as a switch and break the loop and move on to next row
                    shouldSwitch = true;
                    document.getElementById("sortBy").value = "desc";
                    document.getElementById("row").value = "Onderdeelcode";
                    break;
                }
            }
        }
        if (shouldSwitch) {
            //If needs to switch sitch rows and set swithing to true; +1 to switchcount
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            //If no switch is done revert direction of sort and loop again
            if (!switchcount && direction === "asc") {
                direction = "desc";
                switching = true;
            }
        }
    }
}

function SortTableNum(n)
{
    //Get table by id; Set switching to true; Set direction(dir) to asc(ascending)
    let table,
        rows,
        rowsLength,
        switching,
        i,
        currentRow,
        nextRow,
        direction,
        shouldSwitch,
        switchcount = 0;

    table = document.getElementById("table");
    switching = true;
    direction = "asc";

    while (switching) {
        //Set switching to false; Get rows
        switching = false;
        rows = table.rows;
        rowsLength = rows.length;

        for (i = 1; i < rowsLength - 1; i++) {
            //Start by saying there should be no switching
            shouldSwitch = false;

            //Get current row and next to compare
            currentRow = rows[i].getElementsByTagName("td")[n];
            nextRow = rows[i + 1].getElementsByTagName("td")[n];

            //Check if the two rows should switch place
            if (direction === "asc") {
                if (Number(currentRow.innerHTML) > Number(nextRow.innerHTML)) {
                    // If so, mark as a switch and break the loop and move on to next row
                    document.getElementById("sortBy").value = "asc";
                    switch (n) {
                        case 6:
                            document.getElementById("row").value =
                                "Aantal_online";
                            break;
                        case 7:
                            document.getElementById("row").value =
                                "Aantal_verkocht";
                            break;
                        case 8:
                            document.getElementById("row").value = "verschil";
                            break;
                        case 9:
                            document.getElementById("row").value = "score";
                            break;
                    }
                    shouldSwitch = true;
                    break;
                }
            } else if (direction === "desc") {
                if (Number(currentRow.innerHTML) < Number(nextRow.innerHTML)) {
                    // If so, mark as a switch and break the loop and move on to next row
                    document.getElementById("sortBy").value = "desc";
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            //If needs to switch sitch rows and set swithing to true; +1 to switchcount
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            //If no switch is done revert direction of sort and loop again
            if (!switchcount && direction === "asc") {
                direction = "desc";
                switching = true;
            }
        }
    }
}

function SortTablePrice(n)
{
    let table,
        rows,
        rowsLength,
        switching,
        i,
        currentRow,
        nextRow,
        direction,
        shouldSwitch,
        switchcount = 0;
    //Get table by id; Set switching to true; Set direction(dir) to asc(ascending)
    table = document.getElementById("table");
    switching = true;
    direction = "asc";

    while (switching) {
        //Set switching to false; Get rows
        switching = false;
        rows = table.rows;
        rowsLength = rows.length;

        for (i = 1; i < rowsLength - 1; i++) {
            //Start by saying there should be no switching
            shouldSwitch = false;

            //Get current row and next to compare
            currentRow = rows[i].getElementsByTagName("td")[n];
            nextRow = rows[i + 1].getElementsByTagName("td")[n];

            //Check if the two rows should switch place
            if (direction === "asc") {
                if (
                    Number(currentRow.innerHTML.slice(1)) >
                    Number(nextRow.innerHTML.slice(1))
                ) {
                    // If so, mark as a switch and break the loop and move on to next row
                    document.getElementById("sortBy").value = "asc";
                    switch (n) {
                        case 1:
                            document.getElementById("row").value = "Avg";
                            break;
                        case 2:
                            document.getElementById("row").value = "Min";
                            break;
                        case 3:
                            document.getElementById("row").value = "Max";
                            break;
                        case 4:
                            document.getElementById("row").value =
                                "adviesPrijs";
                            break;
                        case 5:
                            document.getElementById("row").value = "provisie";
                            break;
                    }
                    shouldSwitch = true;
                    break;
                }
            } else if (direction === "desc") {
                if (
                    Number(currentRow.innerHTML.slice(1)) <
                    Number(nextRow.innerHTML.slice(1))
                ) {
                    // If so, mark as a switch and break the loop and move on to next row
                    document.getElementById("sortBy").value = "desc";
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            //If needs to switch sitch rows and set swithing to true; +1 to switchcount
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount++;
        } else {
            //If no switch is done revert direction of sort and loop again
            if (!switchcount && direction === "asc") {
                direction = "desc";
                switching = true;
            }
        }
    }
}

function DownloadAs(val)
{
    if (val === "analyse/csv" || val === "analyse/xml") {
        document.forms[0].method = "GET";
    } else {
        document.forms[0].method = "POST";
    }

    document.forms[0].action = val;
}
