<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AnalyseRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(Request $request): array
    {

        $brand = $request->validate([
            'merk' => ['required', 'min:2']
        ]);

        $model = $request->validate([
            'merk'  => ['required', 'min:2'],
            'model' => ['required', 'min:2']
        ]);

        $build = $request->validate([
            'merk'     => ['required', 'min:2'],
            'model'    => ['required', 'min:2'],
            'bouwjaar' => ['required', 'min:2']
        ]);

        return [
            $brand,
            $model,
            $build
        ];
    }
}