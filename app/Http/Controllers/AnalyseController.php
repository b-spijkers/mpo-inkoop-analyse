<?php

namespace App\Http\Controllers;

use App\Http\Requests\AnalyseRequest;
use App\Models\Analyse;
use App\Models\Onderdeel;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
use League\Csv\Writer;
use SplTempFileObject;

class AnalyseController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function calculatePartsData($parts, $partsOnline, $onlineLimit, $soldLimit): iterable
    {
        $totalOnline = 0;
        $totalSold = 0;
        $scarcity = 0;
        $mpoProvision = 0;
        $totalResale = 0;

        //For each object in collection run calculations
        foreach ($parts as $ps) {
            foreach ($partsOnline as $po) {
                if ($ps->omschrijving === $po->omschrijving) {
                    $ps['Aantal_online'] = $po->Aantal_online;
                }
            }

            //Add up to total online and sold for each part
            $totalOnline += $ps->Aantal_online;
            $totalSold += $ps->Aantal_verkocht;

            //Set advisory price based on parts online
            switch ($ps->Aantal_online) {
                case 0:
                case 1:
                    $advisoryPrice = $ps->Max;
                    $scarcity += $advisoryPrice;
                    break;
                case 2:
                case 3:
                    $advisoryPrice = $ps->Max;
                    break;
                case 4:
                    $advisoryPrice = $ps->Avg;
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    $advisoryPrice = $ps->Min;
                    break;
                default:
                    $advisoryPrice = 0;
            }

            //Calculate provision and difference between sold and online
            //Add provision of each part to total MPO provision and advisory price of each part to total resale price
            if (!$advisoryPrice && !$ps->Aantal_verkocht) {
                $provision = 0;
            } else {
                $provision = ($advisoryPrice / 100 * 5) + 5;
            }

            $difference = $ps->Aantal_online - $ps->Aantal_verkocht;
            $mpoProvision += $provision;
            $totalResale += $advisoryPrice;

            //Set variables for score calculations
            //Clarification: Can't be true or false since they need to be added up
            $bigDifference = 0;
            $zeroOnline = 0;
            $sold = 0;
            $online = 1;

            //Check if bigDifference is true or false
            if ($difference < 1) {
                $bigDifference = 1;
            }

            //Check if zeroOnline is true or false
            if (!$ps->Aantal_online) {
                $zeroOnline = 1;
            }

            //Check if Verkocht is true or false
            if ($ps->Aantal_verkocht > $soldLimit) {
                $sold = 1;
            }

            //Check if Online is true or false
            if ($ps->Aantal_online > $onlineLimit) {
                $online = -1;
            }

            //Get score
            if ($ps->Avg === 0 && !$ps->Aantal_online || $ps->Avg && $ps->Aantal_online === 0) {
                $calculatedScore = 3;
                $className = 'scoreGreen';
            } else {
                //Count score and color table cell accordingly
                $calculatedScore = $bigDifference + $zeroOnline + $online + $sold;

                if ($calculatedScore > 1) {
                    $className = 'scoreGreen';
                } elseif ($calculatedScore === 1) {
                    $className = 'scoreOrange';
                } else {
                    $className = 'scoreRed';
                }
            }

            $score = $calculatedScore;

            //Add to each object in collection
            $ps['adviesPrijs'] = !$advisoryPrice ? 0.00 : number_format($advisoryPrice, 2, '.', '');
            $ps['provisie'] = number_format($provision, 2, '.', '');
            $ps['verschil'] = $difference;
            $ps['score'] = $score;
            $ps['className'] = $className;
        }

        return [
            'parts'        => $parts,
            'onlineLimit'  => $onlineLimit,
            'soldLimit'    => $soldLimit,
            'scarcity'     => number_format($scarcity, '2', '.', ''),
            'mpoProvision' => number_format($mpoProvision, '2', '.', ''),
            'totalResale'  => number_format($totalResale, '2', '.', ''),
            'totalOnline'  => $totalOnline,
            'totalSold'    => $totalSold,
        ];
    }

    public function getSoldPartsForMc($model, $build): Collection
    {
        return Onderdeel::selectRaw('DISTINCT onderdeelcode.omschrijving,
            MAX(bestelling_regel.prijs) AS Max,
            MIN(bestelling_regel.prijs) AS Min,
            CAST(AVG(bestelling_regel.prijs) AS DECIMAL(7,2)) AS Avg,
            COUNT(CASE WHEN bestelling.status_bestelling_id = 3 THEN 3 END) AS Aantal_verkocht')
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bouwjaar', 'bouwjaar.bouwjaar_id', '=', 'bouwjaar_onderdeel.bouwjaar_id')
            ->join('onderdeelcode', 'onderdeelcode.onderdeelcode_id', '=', 'onderdeel.onderdeelcode_id')
            ->leftJoin('bestelling_regel', 'bestelling_regel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->leftJoin('bestelling', 'bestelling.bestelling_id', '=', 'bestelling_regel.bestelling_id')
            ->where('bouwjaar.bouwjaar_id', '=', $build)
            ->where('bestelling.status_bestelling_id', '=', '3')
            ->where('bouwjaar.model_id', '=', $model)
            ->where('onderdeel.handelaar_id', '!=', 35)
            ->where('onderdeel.handelaar_id', '!=', 21)
            ->where('onderdeel.soort', '=', '1')
            ->where('onderdeel.updated_at', '>=', config('analyse.getOldDate'))
            ->whereNotIn('onderdeelcode.omschrijving', config('analyse.excludedPartsDescription'))
            ->groupBy('onderdeelcode.omschrijving')
            ->orderBy('onderdeelcode.omschrijving', 'asc')
            ->get();
    }

    public function getTotalPartsOnline($model, $build): Onderdeel
    {
        return Onderdeel::selectRaw('count(*) AS MaxOnline')
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->where('bouwjaar_onderdeel.bouwjaar_id', '=', $build)
            ->where('onderdeel.model_id', '=', $model)
            ->where('onderdeel.onderdeelstatus_id', '=', 3)
            ->first();
    }

    public function getPartsOnline($model, $build): Collection
    {
        return Onderdeel::selectRaw('DISTINCT onderdeelcode.omschrijving,
            COUNT(*) AS Aantal_online')
            ->join('onderdeelcode', 'onderdeelcode.onderdeelcode_id', '=', 'onderdeel.onderdeelcode_id')
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bouwjaar', 'bouwjaar.bouwjaar_id', '=', 'bouwjaar_onderdeel.bouwjaar_id')
            ->where('bouwjaar_onderdeel.bouwjaar_id', '=', $build)
            ->where('bouwjaar.model_id', '=', $model)
            ->where('onderdeel.handelaar_id', '!=', 35)
            ->where('onderdeel.handelaar_id', '!=', 21)
            ->whereNotIn('onderdeel.onderdeelstatus_id', [1, 2, 6, 7, 9, 20])
            ->whereNotIn('onderdeelcode.omschrijving', config('analyse.excludedPartsDescription'))
            ->groupBy('onderdeelcode.omschrijving')
            ->orderBy('onderdeelcode.omschrijving', 'asc')
            ->get();
    }

    public function getAvgDuration($model, $build): int
    {
        $avgDuration = Onderdeel::selectRaw(
            'DATEDIFF(bestelling.datum_bestelling, onderdeel.creatie) AS MaxLooptijd'
        )
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bouwjaar', 'bouwjaar.bouwjaar_id', '=', 'bouwjaar_onderdeel.bouwjaar_id')
            ->join('bestelling_regel', 'bestelling_regel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bestelling', 'bestelling.bestelling_id', '=', 'bestelling_regel.bestelling_id')
            ->where('bouwjaar_onderdeel.bouwjaar_id', '=', $build)
            ->where('bouwjaar.model_id', '=', $model)
            ->where('onderdeel.handelaar_id', '!=', 35)
            ->where('onderdeel.handelaar_id', '!=', 21)
            ->where('bestelling.status_bestelling_id', '=', '3')
            ->where('onderdeel.soort', '=', '1')
            ->where('onderdeel.updated_at', '>=', config('analyse.getOldDate'))
            ->get();

        $avgDuration = $avgDuration->pluck('MaxLooptijd');
        $avgDuration = $avgDuration->toArray();

        $sizeOfAvgDuration = sizeof($avgDuration);
        $avgDuration = array_sum($avgDuration);

        if (!$sizeOfAvgDuration) {
            $avgDuration = 0;
        } else {
            $avgDuration = $avgDuration / $sizeOfAvgDuration;
        }

        return number_format($avgDuration, 0, '.', '');
    }

    public function searchMotorcycle(AnalyseRequest $request)
    {
        //Request motorcycle Brand_id, Model_id and Build_id
        //In each of the values the id and name(omschrijving) is given which are seperated by a '-'
        $brand = $request->merk;
        $brandData = explode('-', $brand);
        $brandName = $brandData[1];
        Session::put('brandName', $brandName);

        $model = $request->model;
        $modelData = explode('-', $model);
        $modelName = $modelData[1];
        $modelId = $modelData[0];
        Session::put('modelName', $modelName);

        $build = $request->bouwjaar;
        $buildData = explode('-', $build);
        $buildYear = $buildData[1] . '-' . $buildData[2];
        $buildDescription = $buildData[3];
        $buildId = $buildData[0];
        Session::put('buildYear', $buildYear);
        Session::put('buildDesc', $buildDescription);

        //Get prices for the parts of specific model and build
        $parts = $this->getSoldPartsForMc($modelId, $buildId);

        //Get total amount of parts online for all parts of selected motorcycle; No dealer or part discrimination
        $totalPartsOnline = $this->getTotalPartsOnline($modelId, $buildId);

        //Get amount parts of online for all parts of selected motorcycle
        $partsOnline = $this->getPartsOnline($modelId, $buildId);

        //Merge parts with all parts online and only get unique values;
        $parts = $parts->mergeRecursive($partsOnline);
        $parts = $parts->unique('omschrijving')->values()->sortBy('omschrijving');

        //Get average duration, in days, for parts being sold
        $avgDuration = $this->getAvgDuration($modelId, $buildId);

        //Get part prices, amount online, amount sold and score
        $partsData = $this->calculatePartsData($parts, $partsOnline, 7, 3);

        //Try to calculate scarcity percentage, if divided by zero no results were found
        if ($partsData['totalResale'] === '0.00' || $partsData['scarcity'] === '0.00') {
            return redirect('/')->withErrors(['error' => 'Helaas, niks gevonden!']);
        } else {
            $scarcityPrc = ($partsData['scarcity'] / $partsData['totalResale']) * 100;
        }

        //Calculate best buy in price
        $bestBuyIn = $partsData['scarcity'] - $partsData['mpoProvision'];

        //If divide by zero set ratio value to opposite value
        if (!$partsData['totalOnline']) {
            $ratio = $partsData['totalSold'];
        } elseif (!$partsData['totalSold']) {
            $ratio = $partsData['totalOnline'];
        } else {
            $ratio = $partsData['totalOnline'] / $partsData['totalSold'];
        }

        //Calculate ratio percentage
        $ratioPrc = $ratio * 100;

        //Check if there is an analysis of current motorcycle existing in database
        $checkForAnalysis = Analyse::select()
            ->where('merk', '=', $brandName)
            ->where('model', '=', $modelName)
            ->where('bouwjaar', '=', $buildYear)
            ->first();
        //If new motorcycle analysis, create new entry
        if (!$checkForAnalysis) {
            $analyse = new Analyse();
            $analyse->merk = $brandName;
            $analyse->model = $modelName;
            $analyse->bouwjaar = $buildYear;
            $analyse->ratio = $ratio;
            $analyse->schaarste = $partsData['scarcity'];
            $analyse->schaarste_prc = $scarcityPrc;
            $analyse->beste_inkoopprijs = $bestBuyIn;
            $analyse->mpo_provisie = $partsData['mpoProvision'];
            $analyse->tot_verkoopadvies = $partsData['totalResale'];
            $analyse->aantal_online = $totalPartsOnline['MaxOnline'];
            $analyse->aantal_verkocht = $partsData['totalSold'];
            $analyse->gem_doorlooptijd = $avgDuration;
            $analyse->save();
        }

        Session::put([
            'onderdeelPrijzen' => $partsData['parts'],
            'onderdelenOnline' => $partsOnline,
            'schaarste'        => $partsData['scarcity'],
            'schaarstePrc'     => number_format($scarcityPrc, 2, '.', ''),
            'inkoopPrijs'      => number_format($bestBuyIn, 2, '.', ''),
            'ratio'            => number_format($ratio, 5, '.', ''),
            'ratioPrc'         => number_format($ratioPrc, 2, '.', ''),
            'totaalOnline'     => $totalPartsOnline,
            'totaalVerkocht'   => $partsData['totalSold'],
            'mpoProvisie'      => $partsData['mpoProvision'],
            'totaalVerkoop'    => $partsData['totalResale'],
            'maxLooptijd'      => $avgDuration,
        ]);

        return view('analyse', [
            'onderdeelPrijzen' => $partsData['parts'],
            'modelName'        => $modelName,
            'brandName'        => $brandName,
            'buildYear'        => $buildYear,
            'buildDesc'        => $buildDescription,
            'schaarste'        => $partsData['scarcity'],
            'schaarstePrc'     => number_format($scarcityPrc, 2, '.', ''),
            'inkoopPrijs'      => number_format($bestBuyIn, 2, '.', ''),
            'ratio'            => number_format($ratio, 5, '.', ''),
            'ratioPrc'         => number_format($ratioPrc, 2, '.', ''),
            'totaalOnline'     => $totalPartsOnline,
            'totaalVerkocht'   => $partsData['totalSold'],
            'mpoProvisie'      => $partsData['mpoProvision'],
            'totaalVerkoop'    => $partsData['totalResale'],
            'maxLooptijd'      => $avgDuration,
        ]);
    }

    public function createCsv(Request $request)
    {
        $parts = session('onderdeelPrijzen');
        $partsOnline = session('onderdelenOnline');
        $totalPartsOnline = session('totaalOnline')['MaxOnline'];
        $scarcityPrc = session('schaarstePrc');
        $avgDuration = session('maxLooptijd');

        //Get analyse_id from table 'analyse'
        $analyseId = Analyse::select('analyse_id')
            ->where('merk', '=', session('brandName'))
            ->where('model', '=', session('modelName'))
            ->where('bouwjaar', '=', session('buildYear'))
            ->first();

        //Get data from database table Analyse
        $analysisData = Analyse::select()->where('analyse_id', '=', $analyseId->analyse_id)->first();
        $analysisData->toArray();

        $parts = $this->calculatePartsData(
            $parts,
            $partsOnline,
            $analysisData->grens_online,
            $analysisData->grens_verkocht
        );

        $ratio = $parts['totalOnline'] / $parts['totalSold'];
        $bestBuyIn = $parts['scarcity'] - $parts['mpoProvision'];

        //Update database table 'analyse' with requested data
        Analyse::where('analyse_id', $analyseId->analyse_id)
            ->update([
                'aankoopprijs'        => (float)$request->buyInPrice,
                'transport_demontage' => (float)$request->transport,
                'marge'               => (float)$request->marginInput,
                'rentevergoeding'     => (float)$request->iRentAYearInput,
                'min_verkoop'         => (float)$request->minResaleInput,
                'min_verkoop_prc'     => (float)$request->minResalePrcInput,
                'aankoopdatum'        => $request->buyInDate,
                'grens_online'        => (int)$request->onlineLimit,
                'grens_verkocht'      => (int)$request->soldLimit,
                'ratio'               => $ratio,
                'schaarste'           => $parts['scarcity'],
                'schaarste_prc'       => $scarcityPrc,
                'beste_inkoopprijs'   => $bestBuyIn,
                'mpo_provisie'        => $parts['mpoProvision'],
                'tot_verkoopadvies'   => $parts['totalResale'],
                'aantal_online'       => $totalPartsOnline,
                'aantal_verkocht'     => $parts['totalSold'],
                'gem_doorlooptijd'    => $avgDuration
            ]);

        //Get current date for filename
        $getDate = date('Y-m-d');
        $filename = $analysisData->merk . '-' . $analysisData->model . '-' . $analysisData->bouwjaar . '_' . $getDate;

        $customMcStatArrayOne = [[
            'Ratio'               => $ratio,
            'Schaarste'           => $parts['scarcity'],
            'Schaarste percentage' => $scarcityPrc,
        ]];

        $customMcStatArrayTwo = [[
            'Beste Inkoopprijs'    => $bestBuyIn,
            'MPO Provisie'         => $parts['mpoProvision'],
            'Totaal Verkoopadvies' => $parts['totalResale']
        ]];

        $customMcStatArrayThree = [[
            'Aantal online'        => $totalPartsOnline,
            'Aantal verkocht'      => $parts['totalSold'],
            'Gem. Doorlooptijd'    => $avgDuration,
        ]];

        //Custom array's for clarity in csv file
        $customMcArray = [[
            'Merk'     => $analysisData->merk,
            'Model'    => $analysisData->model,
            'Bouwjaar' => $analysisData->bouwjaar,
            'Omschrijving' => session('buildDesc')
        ]];

        $customPartsArray = [[
            'Aankoopprijs'    => $analysisData->aankoopprijs,
            'Transport'       => $analysisData->transport_demontage,
            'Aankoopdatum'    => $analysisData->aankoopdatum,
            'Grens online'     => $analysisData->grens_online,
            'Grens verkocht'   => $analysisData->grens_verkocht,
            'Rentevergoeding' => $analysisData->rentevergoeding,
            'Marge'           => $analysisData->marge,
            'Min. verkoop'     => $analysisData->min_verkoop,
            'Min. verkoop_prc' => $analysisData->min_verkoop_prc,
        ]];

        $customHeaderArray = [
            'Omschrijving',
            'Avg',
            'Min',
            'Max',
            'Adviesprijs',
            'Provisie',
            'Aantal online',
            'Aantal verkocht',
            'Verschil',
            'Score',
            'Classname',
        ];
        $customHeaderArray = array_fill_keys($customHeaderArray, 0);

        //Empty array for line break; I don't know how to do it otherwise
        $emptyArray = [['']];

        $csv = Writer::createFromFileObject(new SplTempFileObject());
        $csv->insertOne(array_keys($customMcArray[0]));
        $csv->insertAll($customMcArray);
        $csv->insertAll($emptyArray);
        $csv->insertOne(array_keys($customMcStatArrayOne[0]));
        $csv->insertAll($customMcStatArrayOne);
        $csv->insertAll($emptyArray);
        $csv->insertOne(array_keys($customMcStatArrayTwo[0]));
        $csv->insertAll($customMcStatArrayTwo);
        $csv->insertAll($emptyArray);
        $csv->insertOne(array_keys($customMcStatArrayThree[0]));
        $csv->insertAll($customMcStatArrayThree);
        $csv->insertAll($emptyArray);
        $csv->insertOne(array_keys($customPartsArray[0]));
        $csv->insertAll($customPartsArray);
        $csv->insertAll($emptyArray);

        $parts = $parts['parts'];

        //For a lack of a better solution.
        //Here's a super scuffed way of showing the right data. If it works, it ain't stupid.
        foreach ($parts as $p) {
            $partName = $p->omschrijving;
            $partMax = $p->Max;
            $partMin = $p->Min;
            $partAvg = $p->Avg;
            $partSold = $p->Aantal_verkocht;
            $partOnline = $p->Aantal_online;
            $partAdvPrice = $p->adviesPrijs;
            $partProvision = $p->provisie;
            $partDiff = $p->verschil;
            $partScore = $p->score;
            $partClassName = $p->className;

            unset(
                $p['omschrijving'],
                $p['Max'],
                $p['Min'],
                $p['Avg'],
                $p['Aantal_verkocht'],
                $p['Aantal_online'],
                $p['adviesPrijs'],
                $p['provisie'],
                $p['verschil'],
                $p['score'],
                $p['className']
            );

            $p['omschrijving'] = $partName;
            if (!$partAvg) {
                $p['Avg'] = 0;
            } else {
                $p['Avg'] = $partAvg;
            }
            if (!$partMin) {
                $p['Min'] = 0;
            } else {
                $p['Min'] = $partMin;
            }
            if (!$partMax) {
                $p['Max'] = 0;
            } else {
                $p['Max'] = $partMax;
            }
            $p['adviesPrijs'] = $partAdvPrice;
            $p['provisie'] = $partProvision;
            if (!$partOnline) {
                $p['Aantal_online'] = 0;
            } else {
                $p['Aantal_online'] = $partOnline;
            }
            if (!$partSold) {
                $p['Aantal_verkocht'] = 0;
            } else {
                $p['Aantal_verkocht'] = $partSold;
            }
            $p['verschil'] = $partDiff;
            $p['score'] = $partScore;
            $p['className'] = $partClassName;
        }

        $csv->insertOne(array_keys($customHeaderArray));

        foreach ($parts as $ond) {
            $csv->insertOne($ond->toArray());
        }

        return response((string)$csv, 200, [
            'Content-Type'              => 'text/csv',
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition'       => 'attachment; filename="' . $filename . '.csv"',
        ]);
    }

    public function createPDF(Request $request): Response
    {
        //Get and set all data to variables
        $parts = session('onderdeelPrijzen');
        $ratioPrc = session('ratioPrc');
        $schaarste = session('schaarste');
        $schaarstePrc = session('schaarstePrc');
        $inkoopPrijs = session('inkoopPrijs');
        $ratio = session('ratio');
        $totaalOnline = session('totaalOnline');
        $totaalVerkocht = session('totaalVerkocht');
        $mpoProvisie = session('mpoProvisie');
        $totaalVerkoop = session('totaalVerkoop');
        $brandName = session('brandName');
        $modelName = session('modelName');
        $buildYear = session('buildYear');
        $buildDesc = session('buildDesc');
        $avgLooptijd = session('maxLooptijd');

        //Request additional data from analyse.blade view
        $buyInPrice = $request->buyInPrice;
        $transport = $request->transport;
        $marge = $request->marginInput;
        $rentevergoeding = $request->iRentAYearInput;
        $minVerkoop = $request->minResaleInput;
        $minVerkoopPrc = $request->minResalePrcInput;
        $buyInDate = $request->buyInDate;
        $onlineLimit = $request->onlineLimit;
        $soldLimit = $request->soldLimit;
        $scarcityPrcUp = $request->scarcityPrcUpInput;
        $row = $request->row;
        $sortBy = $request->sortBy;

        //Get date for filename; set filename
        $getDate = date('Y-m-d');
        $filename = session('brandName') . '-' . session('modelName') . '-' . session('buildYear') . '_' . $getDate;

        //Check if sorted is set; if not set, set base value
        if (!$sortBy) {
            $row = 'omschrijving';
            $sortBy = 'asc';
        }

        //Switch sort based on $sortBy value
        if ($sortBy === 'asc') {
            $parts = $parts->sortBy($row);
        } else {
            $parts = $parts->sortByDesc($row);
        }

        //Store all variables in $data array
        $compactPartsData = compact(
            'parts',
            'ratioPrc',
            'schaarste',
            'schaarstePrc',
            'inkoopPrijs',
            'ratioPrc',
            'ratio',
            'totaalOnline',
            'totaalVerkocht',
            'totaalVerkoop',
            'mpoProvisie',
            'brandName',
            'modelName',
            'buildYear',
            'buildDesc',
            'avgLooptijd',
            'filename',
            'buyInPrice',
            'transport',
            'marge',
            'rentevergoeding',
            'minVerkoop',
            'minVerkoopPrc',
            'buyInDate',
            'onlineLimit',
            'soldLimit',
            'scarcityPrcUp'
        );

        //Pass data to view
        view()->share('pdf', $compactPartsData);

        //Load as pdf with data; set orientation to landscape
        $pdf = PDF::loadView('pdf', $compactPartsData)->setPaper('a4');

        return $pdf->stream($filename . '.pdf');
    }
}
