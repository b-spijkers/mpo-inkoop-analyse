<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use App\Models\Models;
use App\Models\Builds;
use App\Models\Onderdeel;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function Index()
    {
        return view('search');
    }

    public function getBrands(): JsonResponse
    {
        $brandData = Brands::orderBy('omschrijving')->get();

        return response()->json($brandData);
    }

    public function getBrandModels(Request $request): JsonResponse
    {
        $brand = $request->brand_id;
        $brandData = explode('-', $brand);
        $brandId = $brandData[0];

        $brandModelData = Models::orderBy('omschrijving')->where('merk_id', $brandId)->get();

        return response()->json($brandModelData);
    }

    public function getModelBuilds(Request $request): JsonResponse
    {
        $model = $request->model_id;
        $modelData = explode('-', $model);
        $modelId = $modelData[0];

        $modelBuildsData = Builds::where('model_id', $modelId)
            ->orderBy('bouwjaar_van', 'asc')
            ->get();

        return response()->json($modelBuildsData);
    }

    public function getSoldPartsForMc($model, $build): Collection
    {
        return Onderdeel::selectRaw('DISTINCT onderdeelcode.omschrijving,
            MAX(bestelling_regel.prijs) AS Max,
            MIN(bestelling_regel.prijs) AS Min,
            CAST(AVG(bestelling_regel.prijs) AS DECIMAL(7,2)) AS Avg,
            COUNT(CASE WHEN bestelling.status_bestelling_id = 3 THEN 3 END) AS Aantal_verkocht')
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bouwjaar', 'bouwjaar.bouwjaar_id', '=', 'bouwjaar_onderdeel.bouwjaar_id')
            ->join('onderdeelcode', 'onderdeelcode.onderdeelcode_id', '=', 'onderdeel.onderdeelcode_id')
            ->leftJoin('bestelling_regel', 'bestelling_regel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->leftJoin('bestelling', 'bestelling.bestelling_id', '=', 'bestelling_regel.bestelling_id')
            ->where('bouwjaar.bouwjaar_id', '=', $build)
            ->where('bouwjaar.model_id', '=', $model)
            ->where('bestelling.status_bestelling_id', '=', '3')
            ->where('onderdeel.handelaar_id', '!=', 35)
            ->where('onderdeel.handelaar_id', '!=', 21)
            ->where('onderdeel.soort', '=', '1')
            ->where('onderdeel.updated_at', '>=', config('analyse.getOldDate'))
            ->whereNotIn('onderdeelcode.omschrijving', config('analyse.excludedPartsDescription'))
            ->groupBy('onderdeelcode.omschrijving')
            ->orderBy('onderdeelcode.omschrijving', 'asc')
            ->get();
    }

    public function getTotalPartsOnlineForMc($model, $build): Onderdeel
    {
        return Onderdeel::selectRaw('count(*) AS MaxOnline')
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->where('bouwjaar_onderdeel.bouwjaar_id', '=', $build)
            ->where('onderdeel.model_id', '=', $model)
            ->where('onderdeel.onderdeelstatus_id', '=', 3)
            ->first();
    }

    public function getPartsOnlineForMc($model, $build): Collection
    {
        return Onderdeel::selectRaw('DISTINCT onderdeelcode.omschrijving,
            COUNT(*) AS Aantal_online')
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bouwjaar', 'bouwjaar.bouwjaar_id', '=', 'bouwjaar_onderdeel.bouwjaar_id')
            ->join('onderdeelcode', 'onderdeelcode.onderdeelcode_id', '=', 'onderdeel.onderdeelcode_id')
            ->where('bouwjaar_onderdeel.bouwjaar_id', '=', $build)
            ->where('bouwjaar.model_id', '=', $model)
            ->where('onderdeel.handelaar_id', '!=', 35)
            ->where('onderdeel.handelaar_id', '!=', 21)
            ->whereNotIn('onderdeel.onderdeelstatus_id', [1, 2, 6, 7, 9, 20])
            ->whereNotIn('onderdeelcode.omschrijving', config('analyse.excludedPartsDescription'))
            ->groupBy('onderdeelcode.omschrijving')
            ->orderBy('onderdeelcode.omschrijving', 'asc')
            ->get();
    }

    public function getAvgDuration($model, $build): int
    {
        $avgDuration = Onderdeel::selectRaw(
            'DATEDIFF(bestelling.datum_bestelling, onderdeel.creatie) AS MaxLooptijd'
        )
            ->join('bouwjaar_onderdeel', 'bouwjaar_onderdeel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bouwjaar', 'bouwjaar.bouwjaar_id', '=', 'bouwjaar_onderdeel.bouwjaar_id')
            ->join('bestelling_regel', 'bestelling_regel.onderdeel_id', '=', 'onderdeel.onderdeel_id')
            ->join('bestelling', 'bestelling.bestelling_id', '=', 'bestelling_regel.bestelling_id')
            ->where('bouwjaar_onderdeel.bouwjaar_id', '=', $build)
            ->where('bouwjaar.model_id', '=', $model)
            ->where('onderdeel.handelaar_id', '!=', 35)
            ->where('onderdeel.handelaar_id', '!=', 21)
            ->where('bestelling.status_bestelling_id', '=', '3')
            ->where('onderdeel.soort', '=', '1')
            ->where('onderdeel.updated_at', '>=', config('analyse.getOldDate'))
            ->get();

        $avgDuration = $avgDuration->pluck('MaxLooptijd');
        $avgDuration = $avgDuration->toArray();

        $sizeOfAvgDuration = sizeof($avgDuration);
        $avgDuration = array_sum($avgDuration);

        if (!$sizeOfAvgDuration) {
            $avgDuration = 0;
        } else {
            $avgDuration = $avgDuration / $sizeOfAvgDuration;
        }

        return number_format($avgDuration, 0, '.', '');
    }

    public function getMcScarcity(Request $request): JsonResponse
    {

        $scarcityLimit = $request->scarcityLim;
        $partsLimit = $request->partsLim;

        $brand = $request->brand_id;
        $brandData = explode('-', $brand);
        $brandId = $brandData[0];

        $model = $request->model_id;
        $modelData = explode('-', $model);
        $modelId = $modelData[0];


        /*This is needed because when you've already selected a brand and model and select a new brand,
        the model_id returns the entire unexploded value e.g: 435-1198   <-(Ducati model).
        If above happens string is always longer than 5, if true only execute query for brand
        */
        $strLen = strlen($modelId);

        if (!$modelId) {
            //Execute query based on brand only
            $motorcycles = Builds::selectRaw('merk.merk_id, merk.omschrijving AS Merk,
            model.model_id,
            model.omschrijving AS Model,
            bouwjaar.bouwjaar_id,
            bouwjaar.bouwjaar_van AS bouwjaar_van,
            bouwjaar.bouwjaar_tot AS bouwjaar_tot')
                ->join('model', 'model.model_id', '=', 'bouwjaar.model_id')
                ->join('merk', 'merk.merk_id', '=', 'model.merk_id')
                ->where('merk.merk_id', '=', $brandId)
                ->orderBy('merk.omschrijving')
                ->get();
        } else {
            //Execute query based on brand and model
            $motorcycles = Builds::selectRaw('merk.merk_id, merk.omschrijving AS Merk,
            model.model_id,
            model.omschrijving AS Model,
            bouwjaar.bouwjaar_id,
            bouwjaar.bouwjaar_van AS bouwjaar_van,
            bouwjaar.bouwjaar_tot AS bouwjaar_tot')
                ->join('model', 'model.model_id', '=', 'bouwjaar.model_id')
                ->join('merk', 'merk.merk_id', '=', 'model.merk_id')
                ->where('merk.merk_id', '=', $brandId)
                ->where('model.model_id', '=', $modelId)
                ->orderBy('merk.omschrijving')
                ->get();
        }

        //For each motorcycle in $motorcycles calculate scarcity, total parts online, total parts sold and total parts
        //Some variables are still present in the foreach for possible later use, depends on feedback
        foreach ($motorcycles as $mc) {
            $partsSold = $this->getSoldPartsForMc($mc->model_id, $mc->bouwjaar_id);
            $totalPartsOnline = $this->getTotalPartsOnlineForMc($mc->model_id, $mc->bouwjaar_id);
            $partsOnline = $this->getPartsOnlineForMc($mc->model_id, $mc->bouwjaar_id);
            $avgDuration = $this->getAvgDuration($mc->model_id, $mc->bouwjaar_id);


            //Set default values
            $scarcity = 0;
            $totalOnline = 0;
            $totalSold = 0;
            $totalResale = 0;
            $mpoProvision = 0;

            $partsSold = $partsSold->mergeRecursive($partsOnline);
            $partsSold = $partsSold->unique('omschrijving')->values()->sortBy('omschrijving');

            //For each object in collection run calculations
            foreach ($partsSold as $ps) {
                foreach ($partsOnline as $po) {
                    if ($ps->omschrijving === $po->omschrijving) {
                        $ps['Aantal_online'] = $po->Aantal_online;
                    }
                }

                //Add up to total online and sold for each part
                $totalOnline += $ps->Aantal_online;
                $totalSold += $ps->Aantal_verkocht;

                //Set advisory price based ps parts online
                switch ($ps->Aantal_online) {
                    case 1:
                    case 0:
                        $advisoryPrice = $ps->Max;
                        $scarcity += $advisoryPrice;
                        break;
                    case 3:
                    case 2:
                        $advisoryPrice = $ps->Max;
                        break;
                    case 4:
                        $advisoryPrice = $ps->Avg;
                        break;
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        $advisoryPrice = $ps->Min;
                        break;
                    default:
                        $advisoryPrice = 0;
                }

                //Calculate provision and difference between sold and online
                //Add provision of each part to total MPO provision & advisory price of each part to total resale price
                $provision = ($advisoryPrice / 100 * 5) + 5;
                $mpoProvision += $provision;
                $totalResale += $advisoryPrice;

                if ($totalResale >= 1 || $scarcity >= 1) {
                    $scarcityPrc = ($scarcity / $totalResale) * 100;
                    $mc['schaarstePrc'] = number_format($scarcityPrc, 2, '.', '');
                    $mc['totaalVerkocht'] = $totalSold;
                    $mc['looptijd'] = $avgDuration;
                }
            }
            $bestBuyIn = $scarcity - $mpoProvision;
            //Calculate best buy in price
            $mc['bestBuyIn'] = number_format($bestBuyIn, 2, '.', '');
            $mc['totaalOnline'] = $totalPartsOnline->MaxOnline;
        }

        $motorcycles = $motorcycles->reject(function ($value) use ($scarcityLimit) {
            return $value->schaarstePrc < $scarcityLimit;
        });

        $motorcycles = $motorcycles->reject(function ($value) use ($partsLimit) {
            return $value->totaalOnline < $partsLimit;
        });

        $motorcycles = $motorcycles->sortByDesc('schaarstePrc');
        $motorcycles = $motorcycles->values();

        return response()->json($motorcycles);
    }
}
