## MPO Inkoop analyse 
**Doel van het project:**

Het doel van dit project is om het MPO inkoop analyse te automatiseren.

---

## Versies:

Versie 1.0: Bestaat uit de standaard functionliteiten van de inkoop analyse

Versie 2.0: Toevoeging van het tonen hoe lang een product online blijft staan

Versie 3.0: Volledige automatisering van het tonen van motoren. Er wordt gekeken naar alle motoren en de schaarste daarvan wordt gecalculeerd. Dit wordt dan op de zoek pagina weergegeven.
Zo zie je altijd welke motor momenteel een goeie aankoop zou kunnen zijn.

---

**Huidige versie: 3.5**
