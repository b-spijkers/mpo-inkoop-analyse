<?php

return [
    'min' => [
        'string' => 'Selecteer een :attribute',
    ],
    'required' => 'Er moet een :attribute worden ingevuld.',
];
