<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="{{ asset("js/calculations.js") }}" defer></script>
    <link rel="stylesheet" href="{{ asset("css/main.css") }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Analyse rappport</title>
</head>
<body class="analyse-page">
<form action="" method="POST" target="_blank">
    @csrf
    <input type="text" id="sortBy" name="sortBy" hidden>
    <input type="text" id="row" name="row" hidden>
    <div id="info-blocks-upper">
        <div id="ratio-block">
            <h3>RATIO</h3>
            <p id="ratioUp">{{ $ratioPrc }}%</p>
        </div>
        <div id="scarcity-block">
            <h3>SCHAARSTE</h3>
            <p id="scarcityPrcUp">{{ $schaarstePrc }}%</p>
        </div>
        <div id="minSale-block">
            <h3>MIN. VERKOOP</h3>
            <p id="minResalePrcUp"></p>
            <input type="text" name="minResalePrcUpInput" id="minResalePrcUpInput" hidden>
        </div>
        <div id="bestBuyIn-block">
            <h3>Beste inkoop prijs</h3>
            <p id="bestBuyIn">€ {{ $inkoopPrijs }}</p>
        </div>
    </div>
    <h1>Inkoop analyse model:</h1>
    <h2 id="motorName">{{ $brandName }} {{ $modelName }} {{ $buildYear }}</h2>
    <h3 id="motorDesc">{{ $buildDesc }}</h3>
    <button class="btn btn-primary" onmouseover="DownloadAs('analyse/csv')" type="submit">Download als CSV</button>
    <button class="btn btn-outline-primary" onmouseover="DownloadAs('analyse/pdf')" type="submit">Download als PDF
    </button>
    <table class="info-blocks table">
        <!-- First info block -->
        <tr>
            <!-- Left side info block -->
            <td>Aankoopprijs motor</td>
            <td>€ <input id="iBuyIn" placeholder="0" type="number" name="buyInPrice" onchange="CalcPrices()"></td>
            <!-- Right side info block -->
            <td>MPO provisie (5 euro + 5%)</td>
            <td>€ <span id="mpoProvision">{{ $mpoProvisie }}</span></td>
        </tr>
        <tr>
            <!-- Left side info block -->
            <td>Transport + demontage</td>
            <td>€ <input id="iTrans" placeholder="0" type="number" name="transport" onchange="CalcPrices()"></td>
            <!-- Right side info block -->
            <td>Benodigde marge (30%)</td>
            <td>€ <span id="margin" name="marge">0</span><input type="text" name="marginInput" id="marginInput" hidden>
            </td>
        </tr>
        <tr>
            <!-- Left side info block -->
            <td>Rentevergoeding per jaar (obv 5%)</td>
            <td>€ <span id="iRentAYear" name="rentevergoeding">0</span><input type="text" name="iRentAYearInput"
                                                                             id="iRentAYearInput" hidden></td>
            <!-- Right side info block -->
            <td>Minimale verkoopprijs</td>
            <td>€ <span id="minResale" name="min_verkoop">0</span><input type="text" name="minResaleInput"
                                                                        id="minResaleInput" hidden></td>
            <td><span id="minResalePrc" name="min_verkoopPrc"></span><input type="text" name="minResalePrcInput"
                                                                            id="minResalePrcInput" hidden></td>
        </tr>
        <!-- Second info block -->
        <tr>
            <!-- Left side info block -->
            <td></td>
            <td></td>
            <!-- Right side info block -->
            <td>Schaars(<2 online)</td>
            <td><span id="scarcity">€ {{ $schaarste }}</span></td>
            <td id="scarcityPrc">{{ $schaarstePrc }}%</td>
        </tr>
        <tr>
            <!-- Left side info block -->
            <td>Aankoopdatum</td>
            <td><input id="iBuyInDate" type="date" name="buyInDate"></td>
            <!-- Right side info block -->
            <td>Totaal verkoopadviesprijs</td>
            <td>€ <span id="totResale">{{ $totaalVerkoop }}</span></td>
        </tr>
        <tr>
            <!-- Left side info block -->
            <td></td>
            <td></td>
            <!-- Right side info block -->
            <td>Gem.doorlooptijd</td>
            <td>{{ $maxLooptijd }}</td>
        </tr>
        <!-- Third info block -->
        <tr>
            <!-- Left side info block -->
            <td></td>
            <td></td>
            <!-- Right side info block -->
            <td>Aantal onderdelen online</td>
            <td><span id="totalOnline">{{ $totaalOnline->MaxOnline }}</span></td>
        </tr>
        <tr>
            <!-- Left side info block -->
            <td>Grenswaarde veel online</td>
            <td><input id="iOnlineLim" type="number" name="onlineLimit" onchange="ChangeLimit()" value="7"></td>
            <!-- Right side info block -->
            <td>Aantal verkocht afgelopen 2 jaar</td>
            <td id="totalSales">{{ $totaalVerkocht }}</td>
        </tr>
        <tr>
            <!-- Left side info block -->
            <td>Grenswaarde veel verkocht</td>
            <td><input id="iSoldLim" type="number" name="soldLimit" onchange="ChangeLimit()" value="3"></td>
            <!-- Right side info block -->
            <td>Ratio:</td>
            <td id="ratio">{{ $ratio }}%</td>
        </tr>
    </table>
</form>
<table class="analyse-results table table-striped table-hover" id="table">
    <tr class="analyse-row">
        <th onclick="SortTable(0)">Onderdeelcode</th>
        <th onclick="SortTablePrice(1)">Gem</th>
        <th onclick="SortTablePrice(2)">Min</th>
        <th onclick="SortTablePrice(3)">Max</th>
        <th onclick="SortTablePrice(4)">Advies prijs</th>
        <th onclick="SortTablePrice(5)">Provisie</th>
        <th class="text-center" onclick="SortTableNum(6)">Aantal online</th>
        <th class="text-center" onclick="SortTableNum(7)">Aantal verkocht</th>
        <th class="text-center" onclick="SortTableNum(8)">Verschil</th>
        <th class="text-center" onclick="SortTableNum(9)">Score</th>
    </tr>
    @foreach($onderdeelPrijzen as $op)
        <tr class="data-row">
            <td>{{ $op->omschrijving }}</td>
            <td class="average">@if(!$op->Avg)€ 0 @else€ {{ $op->Avg }} @endif</td>
            <td class="minimum">@if(!$op->Min)€ 0 @else€ {{ $op->Min }} @endif</td>
            <td class="maximum">@if(!$op->Max)€ 0 @else€ {{ $op->Max }} @endif</td>
            <td class="advisoryPrice">€ {{ $op->adviesPrijs }}</td>
            <td class="provision provisionCalc">€ {{ $op->provisie }}</td>
            <td class="amountOnline text-center">@if(!$op->Aantal_online)0 @else {{ $op->Aantal_online }} @endif</td>
            <td class="amountSold text-center">@if(!$op->Aantal_verkocht)0 @else {{ $op->Aantal_verkocht }} @endif</td>
            <td class="difference text-center">{{ $op->verschil }}</td>
            <td class="score text-center {{ $op->className }}">{{ $op->score }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
