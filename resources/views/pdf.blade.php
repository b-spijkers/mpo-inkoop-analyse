<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $filename }}</title>
</head>
<style>
    <?php include '../public/css/pdf.css';?>
</style>
<body class="analyse-page">
<h1>Inkoop analyse model:</h1>
<h2>{{ $brandName }} {{ $modelName }} {{ $buildYear }}</h2>
<h3>{{ $buildDesc }}</h3>
<table id="info-blocks-upper">
    <tr>
        <td>
            <h3>RATIO</h3>
            <p>{{ $ratioPrc }}%</p>
        </td>
        <td>
            <h3>SCHAARSTE</h3>
            <p>{{ $schaarstePrc }}%</p>
        </td>
        <td>
            <h3>MIN. VERKOOP</h3>
            <p>@if(!$minVerkoopPrc)
                    0%
                @else
                    {{ $minVerkoopPrc }}%
                @endif</p>
        </td>
        <td>
            <h3>Beste inkoop prijs</h3>
            <p>€ {{ $inkoopPrijs }}</p>
        </td>
    </tr>
</table>
<hr>
<table class="info-blocks table">
    <!-- First info block -->
    <tr>
        <td>Aankoopprijs motor:</td>
        <td>@if(!$buyInPrice)
                € 0
            @else
                € {{ $buyInPrice }}
            @endif</td>
        <!-- Right side info block -->
        <td>MPO provisie (5 euro + 5%):</td>
        <td><span>@if(!$mpoProvisie)
                    € 0
                @else
                    € {{ $mpoProvisie }}
                @endif</span></td>
    </tr>
    <tr>
        <td>Transport + demontage:</td>
        <td>@if(!$transport)
                € 0
            @else
                € {{ $transport }}
            @endif</td>
        <!-- Right side info block -->

        <td>Benodigde marge (30%):</td>
        <td><span>@if(!$marge)
                    € 0
                @else
                    € {{ $marge }}
                @endif</span></td>
    </tr>
    <tr>
        <td>Rentevergoeding per jaar (obv 5%):</td>
        <td><span>@if(!$rentevergoeding)
                    € 0
                @else
                    € {{ $rentevergoeding }}
                @endif</span></td>
        <!-- Right side info block -->
        <td>Minimale verkoopprijs:</td>
        <td><span>@if(!$minVerkoop)
                    € 0
                @else
                    € {{ $minVerkoop }}
                @endif</span></td>
        <td>@if(!$minVerkoopPrc)
                0%
            @else
                {{ $minVerkoopPrc }}%
            @endif</td>
    </tr>
    <!-- Second info block -->
    <tr>
        <td></td>
        <td></td>
        <!-- Right side info block -->
        <td>Schaars(minder dan 2 online):</td>
        <td>€<span> {{ $schaarste }}</span></td>
        <td>{{ $schaarstePrc }}%</td>
    </tr>
    <tr>
        <td>Aankoopdatum:</td>
        <td>{{ $buyInDate }}</td>
        <!-- Right side info block -->
        <td>Totaal verkoopadviesprijs:</td>
        <td>€<span> {{ $totaalVerkoop }}</span></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <!-- Right side info block -->
        <td>Gem.doorlooptijd:</td>
        <td>{{ $avgLooptijd }}</td>
    </tr>
    <!-- Third info block -->
    <tr>
        <td></td>
        <td></td>
        <!-- Right side info block -->
        <td>Aantal onderdelen online:</td>
        <td><span>{{ $totaalOnline->MaxOnline }}</span></td>
    </tr>
    <tr>
        <td>Grenswaarde veel online:</td>
        <td>{{ $onlineLimit }}</td>
        <!-- Right side info block -->
        <td>Aantal verkocht afgelopen 2 jaar:</td>
        <td>{{ $totaalVerkocht }}</td>
    </tr>
    <tr>
        <td>Grenswaarde veel verkocht:</td>
        <td>{{ $soldLimit }}</td>
        <!-- Right side info block -->
        <td>Ratio:</td>
        <td>{{ $ratio }}%</td>
    </tr>
</table>
<!-- Adds page break to pdf; End first page-->
<div class="page-break"></div>
<!-- Next page Start -->
<table class="analyse-results table table-striped table-hover">
    <tr class="analyse-row">
        <th>Onderdeelcode</th>
        <th>Gem</th>
        <th>Min</th>
        <th>Max</th>
        <th>Advies prijs</th>
        <th>Provisie</th>
        <th class="text-center">Aantal online</th>
        <th class="text-center">Aantal verkocht</th>
        <th class="text-center">Verschil</th>
        <th class="text-center">Score</th>
    </tr>
    @foreach($parts as $op)
        <tr class="data-row">
            <td>{{ $op->omschrijving }}</td>
            <td class="average">@if(!$op->Avg)
                    € 0
                @else
                    € {{ $op->Avg }}
                @endif</td>
            <td class="minimum">@if(!$op->Min)
                    € 0
                @else
                    € {{ $op->Min }}
                @endif</td>
            <td class="maximum">@if(!$op->Max)
                    € 0
                @else
                    € {{ $op->Max }}
                @endif</td>
            <td class="advisoryPrice">€ {{ $op->adviesPrijs }}</td>
            <td class="provision provisionCalc">€ {{ $op->provisie }}</td>
            <td class="amountOnline text-center">@if(!$op->Aantal_online)
                    0
                @else
                    {{$op->Aantal_online}}
                @endif</td>
            <td class="amountSold text-center">@if(!$op->Aantal_verkocht)
                    0
                @else
                    {{$op->Aantal_verkocht}}
                @endif</td>
            <td class="difference text-center">{{ $op->verschil }}</td>
            <td class="score text-center {{ $op->className }}">{{ $op->score }}</td>
        </tr>
    @endforeach
</table>
</body>
</html>
