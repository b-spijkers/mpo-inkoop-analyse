<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset("css/main.css") }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Search motorcycle</title>
</head>
<script>
    let oudMerk = {!! json_encode(old('merk', [])) !!}
    let oudModel = {!! json_encode(old('model', [])) !!}
</script>
<body class="container">
<div id="app">
    <form action="analyse" method="POST">
        @csrf
        @if ($errors->any())
            <div class="error">{{ $errors->first() }}</div>
        @endif
        <keep-alive>
            <dynamic-search></dynamic-search>
        </keep-alive>
    </form>
</div>
</body>
<script src="{{Mix("js/app.js")}}">
</script>
</html>
